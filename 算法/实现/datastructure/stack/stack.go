package stack

import "errors"

type Stack struct {
	array   []string // 存放栈元素的切片
	maxSize int      // 最大栈元素个数
	top     int      // 栈顶
}

func NewStack(size int) *Stack {
	return &Stack{
		array:   make([]string, size),
		maxSize: size,
		top:     -1, // 初始化
	}
}

// 入栈
func (s *Stack) Push(elem string) error {
	if s.top == s.maxSize-1 {
		return errors.New("is full")
	}
	s.top++
	s.array[s.top] = elem
	return nil
}

// 出栈
func (s *Stack) Pop() (string, error) {
	if s.top == -1 {
		return "", errors.New("is empty")
	}
	elem := s.array[s.top]
	s.top--
	return elem, nil

}

// 检查是否为空
func (s *Stack) IsEmpty() bool {
	return s.top == -1
}

// 检查是否已满
func (s *Stack) IsFull() bool {
	return s.top == s.maxSize-1
}

// 获取顶部元素
func (s *Stack) Peek() string {
	if s.IsEmpty() {
		return ""
	}
	return s.array[s.top]
}

// 重新定义 String 方法，方便输出
func (s *Stack) String() string {
	str := "["
	for i := s.top; i >= 0; i-- {
		str += s.array[i] + " "
	}
	str += "]"
	return str
}

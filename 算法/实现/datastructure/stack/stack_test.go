package stack

import (
	"fmt"
	"testing"
)

func TestStack(t *testing.T) {
	// 创建一个栈
	stack1 := NewStack(3)
	// 入栈
	_ = stack1.Push("one")
	_ = stack1.Push("two")
	_ = stack1.Push("three")

	// 栈满，无法入栈
	err := stack1.Push("four")
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(stack1)

	elem1, _ := stack1.Pop()
	elem2, _ := stack1.Pop()
	elem3, _ := stack1.Pop()

	fmt.Println("出栈:", elem1)
	fmt.Println("出栈:", elem2)
	fmt.Println("出栈:", elem3)

	// 栈空无法出栈
	_, err = stack1.Pop()
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(stack1)
}

package search

import "fmt"

func insertValSearch(nums []int, start, end int, findVal int) int {
	if start > end {
		return -1
	}
	mid := start + (end-start)*(findVal-nums[start])/(nums[end]-nums[start])
	fmt.Printf("mid: %d\n", mid)
	if findVal < nums[mid] {
		return insertValSearch(nums, start, mid-1, findVal)
	} else if findVal > nums[mid] {
		return insertValSearch(nums, mid+1, end, findVal)
	} else {
		return mid
	}
}

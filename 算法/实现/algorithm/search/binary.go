package search

func binarySearch(nums []int, start, end int, findVal int) int {
	if start > end {
		return -1
	}
	mid := (start + end) / 2
	if findVal < nums[mid] {
		return binarySearch(nums, start, mid-1, findVal)
	} else if findVal > nums[mid] {
		return binarySearch(nums, mid+1, end, findVal)
	}
	return mid
}

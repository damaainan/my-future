package main

import "fmt"

func f() int {
	i := 5
	defer func() {
		i++
	}()
	return i // 5
}

func f1() (result int) {
	defer func() {
		result++
	}()
	return 0 // 1
}

func f2() (r int) {
	t := 5
	defer func() {
		t = t + 5
	}()
	return t // 5
}

func f3() (r int) {
	defer func(r int) {
		r = r + 5
	}(r)
	return 1 // 1
}

func main4() {
	fmt.Println(f())
	fmt.Println(f1())
	fmt.Println(f2())
	fmt.Println(f3())
}

package sort

func selectSort(nums []int) {
	if nums == nil {
		return
	}
	length := len(nums)
	for i := 0; i < length-1; i++ {
		minIndex := i
		for j := i + 1; j < length; j++ {
			if nums[minIndex] > nums[j] {
				minIndex = j
			}
		}

		if minIndex != i {
			nums[i], nums[minIndex] = nums[minIndex], nums[i]
		}
	}

}

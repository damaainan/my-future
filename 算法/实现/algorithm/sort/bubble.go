package sort

import (
	"fmt"
)

// 冒泡排序
// 1. 从当前元素起，向前比较每一对相邻元素，若逆序则交换
// 2. 重复此步

func bubbleSort(nums []int) {
	if nums == nil {
		return
	}
	length := len(nums)
	for i := 0; i < length-1; i++ {
		for j := length - 1; j > i; j-- {
			if nums[j] < nums[j-1] {
				nums[j], nums[j-1] = nums[j-1], nums[j]
			}
		}
		fmt.Printf("第 %d 趟排序结果:%v\n", i+1, nums)
	}
}

// 优化
// 某次循环中已经发生交换
func optimizeBubbleSort(nums []int) {
	if nums == nil {
		return
	}
	length := len(nums)
	isChange := false
	for i := 0; i < length-1; i++ {
		for j := length - 1; j > i; j-- {
			if nums[j] < nums[j-1] {
				nums[j], nums[j-1] = nums[j-1], nums[j]
				fmt.Printf("i=%d,j=%d", i, j)
				isChange = true
			}
		}
		fmt.Printf("第 %d 趟排序结果:%v\n", i+1, nums)
		if !isChange {
			break
		} else {
			isChange = false
		}
	}
}

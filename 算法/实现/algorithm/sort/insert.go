package sort

func insertSort(nums []int) {
	if nums == nil {
		return
	}
	length := len(nums)
	for i := 1; i < length; i++ {
		insertIndex := i - 1
		insertValue := nums[i]
		for insertIndex >= 0 && nums[insertIndex] > insertValue {
			nums[insertIndex+1] = nums[insertIndex]
			insertIndex--
		}
		if insertIndex+1 != i {
			nums[insertIndex+1] = insertValue
		}
	}
}

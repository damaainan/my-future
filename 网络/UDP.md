
### UDP特点
* 速度快
采⽤ UDP 协议时，只要应⽤进程将数据传给 UDP， UDP 就会将此数据打包进 UDP 报⽂段并⽴刻传递给⽹络层，然后 TCP 有拥塞控制的功能，它会在发送前判断互联⽹的拥堵情况，如果互联⽹极度阻塞，那么就会抑制 TCP 的发送⽅。使⽤ UDP 的⽬的就是希望实时性
* 无需建立连接
* 无连接状态
* 头部开销小
每个 TCP 报⽂段都有 20 - 60（选项字段最⼤占⽤ 40 字节） 字节的⾸部开销，⽽ UDP 仅仅只有 8 字节的开销

![](./pics/UDP报文结构.jpg)

每个 UDP 报⽂分为 UDP 报头和 UDP 数据区两部分。报头由 4 个 16 位⻓（2 字节）字段组成，分别说明该报⽂的源端⼝、⽬的端⼝、报⽂⻓度和校验值。

* **源端⼝号(Source Port)** :这个字段占据 UDP 报⽂头的前 16 位，通常包含发送数据报的应⽤程序所使⽤的UDP 端⼝。接收端的应⽤程序利⽤这个字段的值作为发送响应的⽬的地址。这个字段是可选项，有时不会设置源端⼝号。没有源端⼝号就默认为 0 ，通常⽤于不需要返回消息的通信中。
* **⽬标端⼝号(Destination Port)** : 表示接收端端⼝，字段⻓为 16 位
* **⻓度(Length)** : 该字段占据 16 位，表示 UDP 数据报⻓度，包含 UDP 报⽂头和 UDP 数据⻓度。因为 UDP报⽂头⻓度是 8 个字节，所以这个值最⼩为 8，最⼤⻓度为 65535 字节。
* **校验和(Checksum)** ： UDP 使⽤校验和来保证数据安全性， UDP 的校验和也提供了差错检测功能，差错检测⽤于校验报⽂段从源到⽬标主机的过程中，数据的完整性是否发⽣了改变。发送⽅的 UDP 对报⽂段中的16 ⽐特字的和进⾏反码运算，求和时遇到的位溢出都会被忽略

### 为什么 UDP 会提供差错检测的功能？
这其实是⼀种 端到端 的设计原则，这个原则说的是要让传输中各种错误发⽣的概率降低到⼀个可以接受的⽔
平。

UDP 不可靠的原因是它虽然提供差错检测的功能，但是对于差错没有恢复能⼒更不会有重传机制。
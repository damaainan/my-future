TCP 会将要传输的数据流分为多个 块(chunk) ，然后向每个 chunk 中添加 TCP 标头，这样就形成了⼀个 TCP段也就是报⽂段。每⼀个报⽂段可以传输的⻓度是有限的，不能超过 最⼤数据⻓度(Maximum Segment Size) ，俗称 MSS 。在报⽂段向下传输的过程中，会经过链路层，链路层有⼀个 Maximum Transmission Unit ，最⼤传输单元 MTU， 即数据链路层上所能通过最⼤数据包的⼤⼩，最⼤传输单元通常与通信接⼝有关。

那么 **MSS** 和 **MTU** 有啥区别呢？

因为计算机⽹络是分层考虑的，这个很重要，不同层的称呼不⼀样，对于传输层来说，称为报⽂段⽽对⽹络层来说就叫做 IP 数据包，所以， **MTU 可以认为是⽹络层能够传输的最⼤ IP 数据包**，⽽ **MSS（Maximum segmentsize）可以认为是传输层的概念，也就是 TCP 数据包每次能够传输的最⼤量**。

### TCP 报⽂段结构
![](./pics/TCP报文结构.jpg)

* 32 ⽐特的 **序号字段(sequence number field)** 和 32 ⽐特的 **确认号字段(acknowledgment numberfield)** 。这些字段被 TCP 发送⽅和接收⽅⽤来实现可靠的数据传输。
* 4 ⽐特的 **⾸部字段⻓度字段(header length field)** ，这个字段指示了以 32 ⽐特的字为单位的 TCP ⾸部⻓度。 TCP ⾸部的⻓度是可变的，但是通常情况下，选项字段为空，所以 TCP ⾸部字段的⻓度是 20 字节。
* 16 ⽐特的 **接受窗⼝字段(receive window field)** ，这个字段⽤于流量控制。它⽤于指示接收⽅能够/愿意接受的字节数量
* 可变的 **选项字段(options field)** ，这部分==最多包含 40 字节==。
* 6 ⽐特的 **标志字段(flag field)** ， 
    1. ACK 标志⽤于指示确认字段中的值是有效的，这个报⽂段包括⼀个对已被成功接收报⽂段的确认； 
    1. RST 、 SYN 、 FIN 标志⽤于连接的建⽴和关闭；
    1. CWR 和 ECE ⽤于拥塞控制； 
    1. PSH 标志⽤于表示⽴刻将数据交给上层处理； 
    1. URG 标志⽤来表示数据中存在需要被上层处理的 紧急 数据。紧急数据最后⼀个字节由 16 ⽐特的 **紧急数据指针字段(urgeent data pointer field)** 指出。⼀般情况下，PSH 和 URG 并没有使⽤。


#### 序号、确认号实现传输可靠性    
![](./pics/TCP报文序号来源.jpg)
⼀个报⽂段的序号就是数据流的字节编号 。第⼀个数据 0 - 1999 的⾸字节编号就是 0 ， 2000 - 3999 的⾸字节编号就是 2000 。。。。。。
然后，每个序号都会被填⼊ TCP 报⽂段⾸部的序号字段中。

##### ⼏种通信模型
* 单⼯通信：单⼯数据传输只⽀持数据在⼀个⽅向上传输；在同⼀时间只有⼀⽅能接受或发送信息，不能实现双向通信，⽐如⼴播、电视等。
* 双⼯通信是⼀种点对点系统，由两个或者多个在两个⽅向上相互通信的连接⽅或者设备组成。双⼯通信模型有两种： 全双⼯(FDX)和半双⼯(HDX)
    * 全双⼯：在全双⼯系统中，连接双⽅可以相互通信，⼀个最常⻅的例⼦就是电话通信。全双⼯通信是两个单⼯通信⽅式的结合，它要求发送设备和接收设备都有独⽴的接收和发送能⼒。
    * 半双⼯：在半双⼯系统中，连接双⽅可以彼此通信，但不能同时通信，⽐如对讲机，只有把按钮按住的⼈才能够讲话，只有⼀个⼈讲完话后另外⼀个⼈才能讲话。

#### 累计确认

![](./pics/TCP确认应答.jpg)
TCP 通过肯定的 确认应答(ACK) 来实现可靠的数据传输，当主机 A 将数据发出之后会等待主机 B 的响应。如果
有确认应答(ACK)，说明数据已经成功到达对端。反之，则数据很可能会丢失。


### 连接管理
**三次握手**
![](./pics/三次握手数据.jpg)
![](./pics/三次握手状态.jpg)
**四次挥手**
![](./pics/四次挥手数据.jpg)
![](./pics/四次挥手状态.jpg)

##### 初始序列号
初始序列号的英⽂名称是Initial sequence numbers (ISN)
初始序列号是随机⽣成的，每⼀个 TCP 连接都会有⼀个不同的初始序列号。 RFC ⽂档指出初始序列号是⼀个 32 位的计数器，每 4 us（微秒） + 1。因为每个 TCP 连接都是⼀个不同的实例，这么安排的⽬的就是为了防⽌出现序列号重叠的情况

##### 为什么要等待 2MSL 呢？
MSL 是 TCP 报⽂段可以存活或者驻留在⽹络中的最⻓时间
**主要是因为两个理由**
* 为了保证最后⼀个响应能够到达服务器，因为在计算机⽹络中，最后⼀个 ACK 报⽂段可能会丢失，从⽽致使客户端⼀直处于 LAST-ACK 状态等待客户端响应。这时候服务器会重传⼀次 FINACK 断开连接报⽂，客户端接收后再重新确认，重启定时器。如果客户端不是 2MSL ，在客户端发送 ACK 后直接关闭的话，如果报⽂丢失，那么双⽅主机会⽆法进⼊ CLOSED 状态。
* 还可以防⽌ 已失效 的报⽂段。客户端在发送最后⼀个 ACK 之后，再经过经过 2MSL，就可以使本链接持续时间内所产⽣的所有报⽂段都从⽹络中消失。从保证在关闭连接后不会有还在⽹络中滞留的报⽂段去骚扰服务器

### 传输控制
**利⽤窗⼝控制提⾼速度**

**窗⼝控制和重发**


## 流量控制
TCP 有 `流量控制服务(flow-control service)` ⽤于消除缓冲区溢出的情况。流量控制是⼀个速度匹配服务，即发送⽅的发送速率与接受⽅应⽤程序的读取速率相匹配。
TCP 通过使⽤⼀个 `接收窗⼝(receive window)` 的变量来提供流量控制。接收窗⼝会给发送⽅⼀个指示**到底还有多少可⽤的缓存空间**。发送端会根据接收端的实际接受能⼒来控制发送的数据量。

接收端主机向发送端主机通知⾃⼰可以接收数据的⼤⼩，发送端会发送不超过这个限度的数据，这个⼤⼩限度就是**窗⼝⼤⼩**，还记得 TCP 的⾸部么，有⼀个接收窗⼝，我们上⾯聊的时候说这个字段⽤于流量控制。它⽤于指示接收⽅能够/愿意接收的字节数量。

发送端主机会定期发送⼀个 窗⼝探测包 ，这个包⽤于探测接收端主机是否还能够接受数据，当接收端的缓冲区⼀
旦⾯临数据溢出的⻛险时，窗⼝⼤⼩的值也随之被设置为⼀个更⼩的值通知发送端，从⽽控制数据发送量。
![](./pics/流量控制示意图.jpg)

## 超时和重传
TCP 的重传有两种⽅式，⼀种是基于 **时间** ，⼀种是基于 **确认信息** ，⼀般通过确认信息要⽐通过时间更加⾼效。

* **超时重传**
TCP 在发送数据时会设置⼀个定时器，如果在定时器指定的时间内未收到确认信息，那么就会触发相应的超时或者基于计时器的重传操作，计时器超时通常被称为重传超时(RTO)。

TCP 在每次重传⼀次报⽂后，其重传时间都会 `加倍` ，这种"间隔时间加倍"被称为 **⼆进制指数补偿(binary exponential backoff)** 。等到间隔时间加倍到 15.5 min 后，

* **快速重传**

在超时重传定时器到期之前，接收收到连续三个相同的 ACK 后，发送端就知道哪个报⽂段丢失了，于是发送⽅会重发这个丢失的报⽂段，这样就不⽤等待重传定时器的到期，⼤⼤提⾼了效率。

#### SACK 


## 滑动窗口
TCP 报⽂段中的窗⼝⼤⼩ **表示接收端还能够接收的缓存空间的⼤⼩**，以字节为单位。这个窗⼝⼤⼩是⼀种动态的，因为⽆时⽆刻都会有报⽂段的接收和消失，这种**动态调整的窗⼝⼤⼩**我们称之为 `滑动窗⼝` 


## 拥塞控制










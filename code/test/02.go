package test

import (
    "fmt"
)

// 循环链表
type Ring struct {
    next, prev *Ring       // 前驱和后驱节点
    Value      interface{} // 数据
}
// 初始化空的循环链表，前驱和后驱都指向自己，因为是循环的
func (r *Ring) init() *Ring {
    r.next = r
    r.prev = r
    return r
}
// 获取下一个节点
func (r *Ring) Next() *Ring {
    if r.next == nil {
        return r.init()
    }
    return r.next
}
// 获取上一个节点
func (r *Ring) Prev() *Ring {
    if r.next == nil {
        return r.init()
    }
    return r.prev
}
// 往节点A，链接一个节点，并且返回之前节点A的后驱节点
func (r *Ring) Link(s *Ring) *Ring {
    n := r.Next()
    if s != nil {
        p := s.Prev()
        r.next = s
        s.prev = r
        n.prev = p
        p.next = n
    }
    return n
}

func linkNewTest() {
     // 第一个节点
     r := &Ring{Value: -1}
     // 链接新的五个节点
     r.Link(&Ring{Value: 1})
     r.Link(&Ring{Value: 2})
     r.Link(&Ring{Value: 3})
     r.Link(&Ring{Value: 4})
     node := r
     for {
         // 打印节点值
         fmt.Println(node.Value)
         // 移到下一个节点
         node = node.Next()
         //  如果节点回到了起点，结束
         if node == r {
             return
         }
     }
 }
// func main() {
//     linkNewTest()
// }
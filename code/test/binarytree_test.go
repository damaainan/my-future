package test

import "testing"

func TestOrder(t *testing.T) {
	o := &TreeNode{Data: "A"}
	o.Left = &TreeNode{Data: "B"}
	o.Right = &TreeNode{Data: "C"}
	o.Left.Left = &TreeNode{Data: "D"}
	o.Left.Right = &TreeNode{Data: "E"}
	o.Right.Left = &TreeNode{Data: "F"}
	order(o)
}
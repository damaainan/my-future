package test

import "math"

func minEatingSpeed(piles []int, H int) int {
	low, high := 1, maxInArray(piles)

	for low<=high {
		mid := low + (high - low) / 2
		res := isPossible(piles, H, mid)
		if !res {
			low = mid + 1
		}else{
			high = high - 1
		}
	}
	return low
}

func isPossible(piles []int, H, k int) bool {
	res := 0
	for _, val := range(piles) {
		res += int(math.Ceil(float64(val) / float64(k)))
	}
	return res <= H
}

func maxInArray(piles []int) int {
	res := 0
	for _, val := range(piles) {
		if val >= res {
			res = val
		}
	}	
	return res
}
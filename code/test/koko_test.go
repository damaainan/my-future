package test

import "testing"
import "fmt"

func TestMinEatingSpeed(t *testing.T) {
	piles1 := []int{3,6,7,11}
	h1 := 8 
	ret1 := minEatingSpeed(piles1, h1)
	fmt.Println("ret====",ret1)

	piles2 := []int{30,11,23,4,20}
	h2 := 5
	ret2 := minEatingSpeed(piles2, h2)
	fmt.Println("ret====",ret2)

	piles3 := []int{30,11,23,4,20}
	h3 := 6
	ret3 := minEatingSpeed(piles3, h3)
	fmt.Println("ret====",ret3)
}
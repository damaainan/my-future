## Innodb Buffer Pool 简介
Buffer Pool 是Innodb 内存中的的一块占比较大的区域，用来缓存表和索引数据。众所周知，从内存访问会比从磁盘访问快很多。为了提高数据的读取速度，Buffer Pool 会通过三种Page 和链表来管理这些经常访问的数据，保证热数据不被置换出Buffer Pool。

### Innodb Buffer Pool的三种Page

Buffer Pool 是按照Page大小来分配，受innodb_page_size控制。

1. Free Page（空闲页）
此Page 未被使用，位于 Free 链表

2. Clean Page（干净页）
此Page 已被使用，但是页面未发生修改，位于LRU 链表。

3. Dirty Page（脏页）
此Page 已被使用，页面已经被修改，其数据和磁盘上的数据已经不一致。当脏页上的数据写入磁盘后，内存数据和磁盘数据一致，那么该Page 就变成了干净页。脏页 同时存在于LRU 链表和Flush 链表。
![](./pics/三种page.jpg)

### 三种链表

1. LRU 链表
![](./pics/LRU链表.jpg)
LRU链表被分成两部分，
一部分是New Sublist(Young 链表)，用来存放经常被读取的页的地址，
另外一部分是Old Sublist(Old 链表)，用来存放较少被使用的页面。
每部分都有对应的头部 和尾部。

**默认情况下**

* Old 链表占整个LRU 链表的比例是3/8。该比例由innodb_old_blocks_pct控制，默认值是37（3/8*100）。该值取值范围为5~95，为全局动态变量。
* 当新的页被读取到Buffer Pool里面的时候，和传统的LRU算法插入到LRU链表头部不同，Innodb LRU算法是将新的页面插入到Yong 链表的尾部和Old 链表的头部中间的位置，这个位置叫做Mid Point，如上图所示。
* 频繁访问一个Buffer Pool的页面，会促使页面往Young链表的头部移动。如果一个Page在被读到Buffer Pool后很快就被访问，那么该Page会往Young List的头部移动，但是如果一个页面是通过预读的方式读到Buffer Pool，且之后短时间内没有被访问，那么很可能在下次访问之前就被移动到Old List的尾部，而被驱逐了。
* 随着数据库的持续运行，新的页面被不断的插入到LRU链表的Mid Point，Old 链表里的页面会逐渐的被移动Old链表的尾部。同时，当经常被访问的页面移动到LRU链表头部的时候，那些没有被访问的页面会逐渐的被移动到链表的尾部。最终，位于Old 链表尾部的页面将被驱逐。




2. Flush 链表
Flush 链表里面保存的都是脏页，也会存在于LRU 链表。
Flush 链表是按照oldest_modification排序，值大的在头部，值小的在尾部
当有页面访被修改的时候，使用mini-transaction，对应的page进入Flush 链表
如果当前页面已经是脏页，就不需要再次加入Flush list，否则是第一次修改，需要加入Flush 链表
当Page Cleaner线程执行flush操作的时候，从尾部开始scan，将一定的脏页写入磁盘，推进检查点，减少recover的时间

3. Free 链表
Free 链表 存放的是空闲页面，初始化的时候申请一定数量的页面
在执行SQL的过程中，每次成功load 页面到内存后，会判断Free 链表的页面是否够用。如果不够用的话，就flush LRU 链表和Flush 链表来释放空闲页。如果够用，就从Free 链表里面删除对应的页面，在LRU 链表增加页面，保持总数不变。


### LRU 链表和Flush链表的区别

### 触发刷脏页的条件



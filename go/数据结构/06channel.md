
可以把一个通道看作是在一个程序内部的一个先进先出（FIFO：first in first out）数据队列。 一些协程可以向此通道发送数据，另外一些协程可以从此通道接收数据。

五种**通道相关的操作**都已经同步过了，因此它们可以在**并发协程中安全运行**而无需其它同步操作。

#### 通道操作详解
通道将被归为三类：

* 零值（nil）通道；
* 非零值但已关闭的通道；
* 非零值并且尚未关闭的通道。
下表简单地描述了三种通道操作施加到三类通道的结果。

操作 | 一个零值nil通道 |	 一个非零值但已关闭的通道 |	一个非零值且尚未关闭的通道
-|-|-|-
关闭 | 产生恐慌 |	产生恐慌	| 成功关闭(C)
发送数据 | 永久阻塞 |	产生恐慌	| 阻塞或者成功发送(B)
接收数据 | 永久阻塞 |	永不阻塞(D)	| 阻塞或者成功接收(A)

对于上表中的五种未打上标的情形，规则很简单：

关闭一个nil通道或者一个已经关闭的通道将产生一个恐慌。
向一个已关闭的通道发送数据也将导致一个恐慌。
向一个nil通道发送数据或者从一个nil通道接收数据将使当前协程永久阻塞。

##### 认为一个通道内部维护了三个队列（均可被视为先进先出队列）
* 接收数据协程队列（可以看做是先进先出队列但其实并不完全是，见下面解释）。此队列是一个没有长度限制的链表。 
* 发送数据协程队列（可以看做是先进先出队列但其实并不完全是，见下面解释）。此队列也是一个没有长度限制的链表。
* 数据缓冲队列。这是一个循环队列（绝对先进先出），它的长度为此通道的容量。此队列中存放的值的类型都为此通道的元素类型。


**每个通道内部维护着一个互斥锁用来在各种通道操作中防止数据竞争**

#### for-range应用于通道

#### select-case分支流程控制代码块
所有的非阻塞case操作中将有一个被**随机选择执行**（而不是按照从上到下的顺序），然后执行此操作对应的case分支代码块



### 设计原理
Go 语言中最常见的、也是经常被人提及的设计模式就是：**不要通过共享内存的方式进行通信，而是应该通过通信的方式共享内存**

##### 先入先出
目前的 Channel 收发操作均遵循了先进先出的设计，具体规则如下：
* 先从 Channel 读取数据的 Goroutine 会先接收到数据；
* 先向 Channel 发送数据的 Goroutine 会得到先发送数据的权利；
带缓冲区和不带缓冲区的 Channel 都会遵循先入先出发送和接收数据

**乐观并发控制也叫乐观锁，很多人都会误以为乐观锁是与悲观锁差不多，然而它并不是真正的锁，只是一种并发控制的思想**


### 数据结构
```go
type hchan struct {
	qcount   uint
	dataqsiz uint
	buf      unsafe.Pointer
	elemsize uint16
	closed   uint32
	elemtype *_type
	sendx    uint
	recvx    uint
	recvq    waitq
	sendq    waitq

	lock mutex
}
```
runtime.hchan 结构体中的五个字段 qcount、dataqsiz、buf、sendx、recv 构建底层的循环队列：

* qcount — Channel 中的元素个数；
* dataqsiz — Channel 中的循环队列的长度；
* buf — Channel 的缓冲区数据指针；
* sendx — Channel 的发送操作处理到的位置；
* recvx — Channel 的接收操作处理到的位置；
除此之外，elemsize 和 elemtype 分别表示当前 Channel 能够收发的元素类型和大小；
sendq 和 recvq 存储了当前 Channel 由于缓冲区空间不足而阻塞的 Goroutine 列表，这些等待队列使用双向链表 runtime.waitq 表示，链表中所有的元素都是 runtime.sudog 结构：

```go
type waitq struct {
	first *sudog
	last  *sudog
}
```
runtime.sudog 表示一个在等待列表中的 Goroutine，该结构中存储了两个分别指向前后 runtime.sudog 的指针以构成链表。



### 发送数据
* 当存在等待的接收者时，通过 runtime.send 直接将数据发送给阻塞的接收者；
* 当缓冲区存在空余空间时，将发送的数据写入 Channel 的缓冲区；
* 当不存在缓冲区或者缓冲区已满时，等待其他 Goroutine 从 Channel 接收数据；

##### 直接发送
如果目标 Channel 没有被关闭并且已经有处于读等待的 Goroutine，那么 runtime.chansend 会从接收队列 recvq 中取出最先陷入等待的 Goroutine 并直接向它发送数据



### 接收数据





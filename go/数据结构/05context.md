
##### Context 本质上是一种在 API 间树形嵌套调用时传递信号的机制

### Context 接口
Context 接口如下：
```go
// Context 用以在多 API 间传递 deadline、cancelation 信号和请求的键值对。
// Context 中的方法能够安全的被多个 goroutine 并发调用。
type Context interface {
    // Done 返回一个只读 channel，该 channel 在 Context 被取消或者超时时关闭
    Done() <-chan struct{}

    // Err 返回 Context 结束时的出错信息
    Err() error

    // 如果 Context 被设置了超时，Deadline 将会返回超时时限。
    Deadline() (deadline time.Time, ok bool)

    // Value 返回关联到相关 Key 上的值，或者 nil.
    Value(key interface{}) interface{}
}
```
* `Done()` 方法返回一个制度的 channel , 当  Context 被主动取消或超时自动取消时，该 Context 所有派生 Context 的 done channel 都被 close 。
* `Err()` 在上述 channel 被 close 前会返回 nil，在被 close 后会返回该 Context 被关闭的信息，error 类型，只有两种，被取消或者超时：
```go
var Canceled = errors.New("context canceled")
var DeadlineExceeded error = deadlineExceededError{}
```
* `Deadline()`
* `Value()` 返回绑定在该 Context 链（我称为回溯链，下文会展开说明）上的给定的 Key 的值，如果没有，则返回 nil。注意，不要用于在函数中传参，其本意在于共享一些横跨整个 Context 生命周期范围的值。

### Context 派生
Context 设计之妙在于可以从已有 Context 进行树形派生，以管理一组过程的生命周期。单个 Context 实例是不可变的，但可以通过 context 包提供的三种方法：WithCancel 、 WithTimeout 和 WithValue 来进行派生并附加一些属性（可取消、时限、键值），以构造一组树形组织的 Context。

![](../pics/context_01派生.jpg)

https://zhuanlan.zhihu.com/p/163684835 TODO 
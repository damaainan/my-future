// 有三个函数分别打印，“dog”，“cat”，“fish”，

// 要求每个函数起一个goroutine，请按照dog，cat，fish的顺序，打印四次，输出到控制台。

package main

import (
	"fmt"
	"sync"
)

var dog = make(chan struct{}, 1)
var cat = make(chan struct{}, 1)
var fish = make(chan struct{}, 1)

var wg sync.WaitGroup

func printDog() {
	wg.Add(1)
	defer wg.Done()
	defer close(dog)
	for i := 0; i < 4; i++ {
		<-fish
		fmt.Println("dog")
		dog <- struct{}{}
	}
}
func printCat() {
	wg.Add(1)
	defer wg.Done()
	defer close(cat)
	for i := 0; i < 4; i++ {
		<-dog
		fmt.Println("cat")
		cat <- struct{}{}
	}
}
func printFish() {
	wg.Add(1)
	defer wg.Done()
	defer close(fish)
	for i := 0; i < 4; i++ {
		<-cat
		fmt.Println("fish")
		fish <- struct{}{}
	}
}

func main() {
	fish <- struct{}{}
	go printDog()
	go printCat()
	go printFish()
	wg.Wait()
}
